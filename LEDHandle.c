#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define LED3_PATH "/sys/class/leds/beaglebone:green:usr0"

void writeLED(char filename[], char value[]){
FILE* fp;
// create a file pointer fpChapter 5 ■ Practical BeagleBone Programming
char fullFileName[100]; // to store the path and filename
sprintf(fullFileName, LED3_PATH "%s", filename); // write path/name
fp = fopen(fullFileName, "w+"); // open file for writing
fprintf(fp, "%s", value); // send the value to the file
fclose(fp); // close the file using the file pointer
}
void removeTrigger(){
writeLED("/trigger", "none");
}
