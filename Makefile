obj-m+=bbb_Driver.o

KVER := $(shell uname -r)
PWD :=$(shell pwd)
CC = gcc

all:
	make -C /lib/modules/$(KVER)/build M=$(PWD) modules
	$(CC) user_prog.c -o tester

clean:
	make -C /lib/modules/$(KVER)/build/ M=$(PWD) clean
